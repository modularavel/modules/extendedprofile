<?php

namespace Modules\ExtendedProfile\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Modules\ExtendedProfile\Events\AffiliateRegistered;

class AffiliateRegistrationGreetings implements ShouldQueue
{
    public function __construct()
    {
    }

    public function handle(AffiliateRegistered $event): void
    {
        // invia l'email di benvenuto al nuovo affiliato
    }
}
