@php $editing = isset($affiliateProfile) @endphp

<div class="flex flex-wrap">

    @if (!$editing)
        <x-inputs.group class="w-full">
            <x-inputs.text
                name="codice_parent"
                label="{{__('extendedprofile::crud.affiliate_profiles.inputs.promo_code')}}"
                :value="old('codice_parent', ($editing ? $affiliateProfile->codice_parent : ''))"
                maxlength="16"
                placeholder="Codice promozione affiliato"
            ></x-inputs.text>
        </x-inputs.group>
    @endif

    <x-inputs.group class="w-full">
        <x-inputs.text
            name="codice_fiscale"
            label="{{__('extendedprofile::crud.affiliate_profiles.inputs.codice_fiscale')}}"
            :value="old('codice_fiscale', ($editing ? $affiliateProfile->codice_fiscale : ''))"
            maxlength="16"
            placeholder="Codice Fiscale"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="w-full">
        <x-inputs.text
            name="partita_iva"
            label="{{__('extendedprofile::crud.affiliate_profiles.inputs.partita_iva')}}"
            :value="old('partita_iva', ($editing ? $affiliateProfile->partita_iva : ''))"
            maxlength="11"
            placeholder="Partita Iva"
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="w-full">
        <x-inputs.text
            name="indirizzo"
            label="{{__('extendedprofile::crud.affiliate_profiles.inputs.indirizzo')}}"
            :value="old('indirizzo', ($editing ? $affiliateProfile->indirizzo : ''))"
            maxlength="256"
            placeholder="Indirizzo"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="w-full">
        <x-inputs.text
            name="cap"
            label="{{__('extendedprofile::crud.affiliate_profiles.inputs.cap')}}"
            :value="old('cap', ($editing ? $affiliateProfile->cap : ''))"
            maxlength="5"
            placeholder="Cap"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="w-full">
        <x-inputs.text
            name="localita"
            label="{{__('extendedprofile::crud.affiliate_profiles.inputs.localita')}}"
            :value="old('localita', ($editing ? $affiliateProfile->localita : ''))"
            maxlength="128"
            placeholder="Localita"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="w-full">
        <x-inputs.text
            name="provincia"
            label="{{__('extendedprofile::crud.affiliate_profiles.inputs.provincia')}}"
            :value="old('provincia', ($editing ? $affiliateProfile->provincia : ''))"
            maxlength="2"
            placeholder="Provincia"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="w-full">
        <x-inputs.text
            name="descrizione"
            label="{{__('extendedprofile::crud.affiliate_profiles.inputs.descrizione')}}"
            :value="old('descrizione', ($editing ? $affiliateProfile->descrizione : ''))"
            maxlength="255"
            placeholder="Descrizione"
            required
        ></x-inputs.text>
    </x-inputs.group>
</div>
