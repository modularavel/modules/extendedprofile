@section('module-resources')
    <!-- bingo -->
    @vite([
                'Modules/ExtendedProfile/Resources/assets/sass/app.scss',
                ])
@endsection

@php use Modules\ExtendedProfile\Models\AffiliateProfile; @endphp
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            @lang('extendedprofile::crud.affiliate_profiles.show_title')
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <x-partials.card>
                <x-slot name="title">
                    <a href="{{ route('affiliate-profiles.index') }}"><i class="mr-1 icon ion-md-arrow-back"></i></a>
                </x-slot>

                <div class="mt-4 px-4">
                    <div class="mb-4">
                        <h4 class="font-bold text-gray-700">
                            @lang('extendedprofile::crud.affiliate_profiles.inputs.user_id')
                        </h4>
                        <span>{{ optional($affiliateProfile->user)->name ?? '-' }}</span>
                    </div>
                    <div class="mb-4">
                        <h5 class="font-medium text-gray-700">
                            @lang('extendedprofile::crud.affiliate_profiles.inputs.promo_code')
                        </h5>
                        <button class="button affiliate-code">{{ $affiliateProfile->codice ?? '-' }}</button>
                    </div>

                    <div class="mb-4">
                        <h5 class="font-medium text-gray-700">
                            @lang('extendedprofile::crud.affiliate_profiles.inputs.codice_fiscale')
                        </h5>
                        <span>{{ $affiliateProfile->codice_fiscale ?? '-' }}</span>
                    </div>
                    <div class="mb-4">
                        <h5 class="font-medium text-gray-700">
                            @lang('extendedprofile::crud.affiliate_profiles.inputs.partita_iva')
                        </h5>
                        <span>{{ $affiliateProfile->partita_iva ?? '-' }}</span>
                    </div>
                    <div class="mb-4">
                        <h5 class="font-medium text-gray-700">
                            @lang('extendedprofile::crud.affiliate_profiles.inputs.indirizzo')
                        </h5>
                        <span>{{ $affiliateProfile->indirizzo ?? '-' }}</span>
                    </div>
                    <div class="mb-4">
                        <h5 class="font-medium text-gray-700">
                            @lang('extendedprofile::crud.affiliate_profiles.inputs.cap')
                        </h5>
                        <span>{{ $affiliateProfile->cap ?? '-' }}</span>
                    </div>
                    <div class="mb-4">
                        <h5 class="font-medium text-gray-700">
                            @lang('extendedprofile::crud.affiliate_profiles.inputs.localita')
                        </h5>
                        <span>{{ $affiliateProfile->localita ?? '-' }}</span>
                    </div>
                    <div class="mb-4">
                        <h5 class="font-medium text-gray-700">
                            @lang('extendedprofile::crud.affiliate_profiles.inputs.provincia')
                        </h5>
                        <span>{{ $affiliateProfile->provincia ?? '-' }}</span>
                    </div>
                    <div class="mb-4">
                        <h5 class="font-medium text-gray-700">
                            @lang('extendedprofile::crud.affiliate_profiles.inputs.descrizione')
                        </h5>
                        <span>{{ $affiliateProfile->descrizione ?? '-' }}</span>
                    </div>
                </div>

                @include('extendedprofile::app.affiliate_profiles.parent-data')

                <div class="mt-10">
                    <a href="{{ route('affiliate-profiles.index') }}" class="button">
                        <i class="mr-1 icon ion-md-return-left"></i>
                        @lang('crud.common.back')
                    </a>

                    @can('create', AffiliateProfile::class)
                        <a href="{{ route('affiliate-profiles.create') }}" class="button">
                            <i class="mr-1 icon ion-md-add"></i>
                            @lang('crud.common.create')
                        </a>
                    @endcan
                </div>
            </x-partials.card>

        </div>
    </div>
</x-app-layout>
