<?php
$parent_affiliate = $affiliateProfile->parentAffiliateProfile;
?>
<div class="mt-4 px-4">
    <div class="mb-4">
        <h4 class="font-bold text-gray-700">
            @lang('extendedprofile::crud.affiliate_profiles.parent_profile')
        </h4>
    </div>

    @if ($parent_affiliate)

        <div class="mb-4">
            <h5 class="font-medium text-gray-700">
                @lang('extendedprofile::crud.affiliate_profiles.name')
            </h5>
            <span>{{ $parent_affiliate->user->name ?? '-' }}</span>
        </div>
        <div class="mb-4">
            <h5 class="font-medium text-gray-700">
                @lang('extendedprofile::crud.affiliate_profiles.inputs.promo_code')
            </h5>
            <button class="button affiliate-code parent">{{ $parent_affiliate->codice ?? '-' }}</button>
        </div>
        <div class="mb-4">
            <h5 class="font-medium text-gray-700">
                @lang('extendedprofile::crud.affiliate_profiles.inputs.descrizione')
            </h5>
            <span>{{ $parent_affiliate->descrizione ?? '-' }}</span>
        </div>

    @else
        <div class="mb-4">
            <h5 class="font-medium text-gray-700">
                @lang('extendedprofile::crud.affiliate_profiles.no_parent_profile')
            </h5>
        </div>
    @endif

</div>
