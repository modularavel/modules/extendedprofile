<?php

return [
    'affiliate_profiles' => [
        'name' => 'Affiliate Profiles',
        'index_title' => 'AffiliateProfiles List',
        'new_title' => 'New Affiliate profile',
        'create_title' => 'Create AffiliateProfile',
        'edit_title' => 'Edit AffiliateProfile',
        'show_title' => 'Show AffiliateProfile',
        'inputs' => [
            'user_id' => 'User',
            'codice_fiscale' => 'Codice Fiscale',
            'partita_iva' => 'Partita Iva',
            'indirizzo' => 'Indirizzo',
            'cap' => 'Cap',
            'localita' => 'Localita',
            'provincia' => 'Provincia',
            'descrizione' => 'Descrizione',
        ],
    ],
];
