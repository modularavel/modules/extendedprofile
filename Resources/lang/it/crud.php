<?php

return [

    'affiliate_profiles' => [
        'name' => 'Nome',
        'index_title' => 'AffiliateProfiles List',
        'new_title' => 'New Affiliate profile',
        'create_title' => 'Create AffiliateProfile',
        'edit_title' => 'Edit AffiliateProfile',
        'show_title' => 'Show AffiliateProfile',
        'parent_profile' => 'Affiliato padre',
        'no_parent_profile' => 'Non ha un profilo affiliato padre.',
        'already_has_profile' => 'L\'utente ha già un Profilo affiliato',
        'invalid_parent_code' => 'Codice parent ":codice" non trovato. Inserire un codice esistente o lasciare vuoto il campo.',
        'inputs' => [
            'user_id' => 'User',
            'codice_fiscale' => 'Codice Fiscale',
            'partita_iva' => 'Partita Iva',
            'indirizzo' => 'Indirizzo',
            'cap' => 'Cap',
            'localita' => 'Localita',
            'provincia' => 'Provincia',
            'descrizione' => 'Descrizione',
            'promo_code' => 'Codice promozione',
        ],
    ],
];
