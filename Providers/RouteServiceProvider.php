<?php
declare(strict_types=1);

namespace Modules\ExtendedProfile\Providers;

use Modules\IRCore\Providers\AbstractRouteServiceProvider;

class RouteServiceProvider extends AbstractRouteServiceProvider
{
    protected string $moduleName = 'ExtendedProfile';
}
