<?php

namespace Modules\ExtendedProfile\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Modules\ExtendedProfile\Models\AffiliateProfile;
use Modules\ExtendedProfile\Policies\AffiliateProfilePolicy;

class AuthServiceProvider extends ServiceProvider
{
    //public function register(): void
    //{

    //}

    protected $policies = [
        AffiliateProfile::class => AffiliateProfilePolicy::class,
    ];

    public function boot()
    {
        /*
                // Automatically finding the Policies
                Gate::guessPolicyNamesUsing(function ($modelClass) {
                    return '\\Modules\\ExtendedProfile\\Policies\\' . class_basename($modelClass) . 'Policy';
                });
        */
        //  $this->registerPolicies();
        /*
                // Implicitly grant "Super Admin" role all permission checks using can()
                Gate::before(function ($user, $ability) {
                    return $user->isSuperAdmin();
                });
        */
    }
}
