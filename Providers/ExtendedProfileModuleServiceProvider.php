<?php
declare(strict_types=1);

namespace Modules\ExtendedProfile\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\IRCore\Providers\AbstractModuleServiceProvider;

class ExtendedProfileModuleServiceProvider extends AbstractModuleServiceProvider
{

    protected string $moduleName = 'ExtendedProfile';
    protected string $moduleNameLower = 'extendedprofile';

    public function register(): ServiceProvider
    {
        return $this->app->register(RouteServiceProvider::class);
    }

}
