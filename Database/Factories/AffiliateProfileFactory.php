<?php

namespace Modules\ExtendedProfile\Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Modules\ExtendedProfile\Models\AffiliateProfile;

class AffiliateProfileFactory extends Factory
{
    protected $model = AffiliateProfile::class;

    public function definition(): array
    {
        return [
            'codice_fiscale' => strtoupper(
                $this->faker->bothify('??????##?##?###?')
            ),
            'partita_iva' => $this->faker->numerify('###########'),
            'indirizzo' => strtolower($this->faker->address(128)),
            'cap' => $this->faker->randomNumber(5),
            'localita' => $this->faker->city(128),
            'provincia' => strtoupper(substr($this->faker->text(5), 0, 2)),
            'descrizione' => $this->faker->text(32),
            'codice' => AffiliateProfile::makeCodice(),
        ];
    }
}
