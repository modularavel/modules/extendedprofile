<?php

namespace Modules\ExtendedProfile\Database\Seeders;

use App\Models\User;
use Database\Seeders\PermissionsSeeder as Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionsSeeder extends Seeder
{
    public function run(): void
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        $permissions = $this->createPermissions(['affiliate-profiles'], $this->actions);

        $role = Role::create(['name' => 'affiliate-admin']);
        $role->givePermissionTo($permissions);
        $user = User::whereEmail('affiliate-admin@' . $this->domain)->first();
        $user?->assignRole($role);

        $role = Role::create(['name' => 'affiliate']);
        $user = User::whereEmail('affiliate@' . $this->domain)->first();
        $user?->assignRole($role);

        $role = Role::findByName('super-admin');
        $role?->givePermissionTo($permissions);
        $user = User::whereEmail('superadmin@' . $this->domain)->first();
        if ($user) {
            $user->assignRole($role);
        }

        $role = Role::findByName('admin');
        $role?->givePermissionTo($permissions);
        $user = User::whereEmail('admin@' . $this->domain)->first();
        if ($user) {
            $user->assignRole($role);
        }

    }
}
