<?php

namespace Modules\ExtendedProfile\Database\Seeders;

use Illuminate\Database\Seeder;

class ExtendedProfileDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PermissionsSeeder::class);
        $this->call(AffiliateProfileSeeder::class);
    }
}
