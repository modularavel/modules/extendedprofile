<?php

namespace Modules\ExtendedProfile\Database\Seeders;

use App\Models\User;
use Hash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\ExtendedProfile\Models\AffiliateProfile;

class AffiliateProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    protected string $domain = '@inforisorse.it';

    public function run(): void
    {
        Model::unguard();

        $affiliate = $this->insertAffiliate('Affiliate Admin');
        $primo = $this->insertAffiliate('Affiliato Primo');
        $secondo = $this->insertAffiliate('Affiliato Secondo');
        $terzo = $this->insertAffiliate('Affiliato Terzo',);

        $affiliate = $this->insertAffiliate('Affiliato Quarto', $primo->id);
        $affiliate = $this->insertAffiliate('Affiliato Quinto', $primo->id);
        $affiliate = $this->insertAffiliate('Affiliato Sesto', $secondo->id);

    }

    protected function insertAffiliate(string $name, int $parent_affiliate_profile_id = null): AffiliateProfile
    {
        $user = User::factory()
            ->count(1)
            ->create([
                'name' => $name,
                'email' => $this->nameToEmail($name),
                'password' => Hash::make('password'),
            ])->first();

        $affiliate = AffiliateProfile::factory()
            ->count(1)
            ->create([
                'user_id' => $user->id,
                'parent_affiliate_profile_id' => $parent_affiliate_profile_id,
            ])->first();
        return $affiliate;
    }

    protected function nameToEmail(string $name): string
    {
        return str_replace(' ', '', strtoupper(trim($name))) . $this->domain;
    }
}
