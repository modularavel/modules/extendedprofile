<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('affiliate_profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('parent_affiliate_profile_id')->nullable();
            $table->string('codice', 12)->unique()->nullable();
            $table->string('codice_fiscale', 16)->unique();
            $table
                ->string('partita_iva', 11)
                ->nullable()
                ->unique();
            $table->string('indirizzo', 256);
            $table->string('cap', 5);
            $table->string('localita', 128);
            $table->string('provincia', 2);
            $table->string('descrizione');

            $table->index('codice_fiscale');
            $table->index('partita_iva');
            $table->index('codice');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('affiliate_profiles');
    }
};
