<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('customer_profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('parent_customer_profile_id')->nullable();
            $table->unsignedBigInteger('reference_affiliate_profile_id')->nullable();
            $table->string('codice', 12)->unique()->nullable();
            $table->string('codice_fiscale', 16)->unique();
            $table
                ->string('partita_iva', 11)
                ->nullable()
                ->unique();
            $table->string('indirizzo', 256);
            $table->string('cap', 5);
            $table->string('localita', 128);
            $table->string('provincia', 2);
            $table->string('descrizione');

            $table->index('codice_fiscale');
            $table->index('partita_iva');
            $table->index('codice');

            $table->timestamps();
            $table->softDeletes();

            $table
                ->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

            $table
                ->foreign('parent_customer_profile_id')
                ->references('id')
                ->on('customer_profiles')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');

            $table
                ->foreign('reference_affiliate_profile_id')
                ->references('id')
                ->on('affiliater_profiles')
                ->onUpdate('CASCADE')
                ->onDelete('CASCADE');
        });
    }

    public function down(): void
    {
        Schema::table('customer_profiles', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['parent_affiliate_profile_id']);
        });
        Schema::dropIfExists('customer_profiles');
    }
};
