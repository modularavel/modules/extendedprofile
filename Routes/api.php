<?php

use Modules\ExtendedProfile\Http\Controllers\Api\AffiliateProfileAffiliateProfilesController;
use Modules\ExtendedProfile\Http\Controllers\Api\AffiliateProfileController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::name('api.')->middleware('auth:sanctum')
    ->group(function () {
        Route::apiResource(
            'affiliate-profiles',
            AffiliateProfileController::class
        );

        // AffiliateProfile Affiliate Profiles
        Route::get(
            '/affiliate-profiles/{affiliateProfile}/affiliate-profiles',
            [AffiliateProfileAffiliateProfilesController::class, 'index']
        )->name('affiliate-profiles.affiliate-profiles.index');
        Route::post(
            '/affiliate-profiles/{affiliateProfile}/affiliate-profiles',
            [AffiliateProfileAffiliateProfilesController::class, 'store']
        )->name('affiliate-profiles.affiliate-profiles.store');
    });
