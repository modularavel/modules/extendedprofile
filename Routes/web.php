<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('extendedprofile')->group(function () {
    Route::get('/', 'ExtendedProfileController@index');
});
Route::prefix('/')
    ->middleware(['auth:sanctum', 'verified'])
    ->group(function () {
        Route::resource(
            'affiliate-profiles',
            AffiliateProfileController::class
        );
    });
