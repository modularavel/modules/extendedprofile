<?php
declare(strict_types=1);

namespace Modules\ExtendedProfile\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;
use Modules\ExtendedProfile\Models\AffiliateProfile;

class AffiliateProfilePolicy
{
    use HandlesAuthorization;

    /*
        public function before(User $user, string $ability): Response
        {
            return $user->hasRole(['admin','affiliatesAdmin'])
                ? Response::allow()
                : Response::denyAsNotFound();
        }
    */
    /**
     * Determine whether the affiliateProfile can view any models.
     */
    public function viewAny(User $user): Response
    {
        return $user->hasPermissionTo('list affiliate-profiles')
            ? Response::allow()
            : Response::denyAsNotFound();
    }

    public function view(User $user, AffiliateProfile $model): Response
    {
        return (bool)AffiliateProfile::findByUserId($user->id) &&
        $user->id == $model->user->id
            ? Response::allow()
            : Response::denyAsNotFound();

    }

    public function create(User $user): Response
    {
        return !AffiliateProfile::findByUserId($user->id)
            ? Response::allow()
            : Response::denyAsNotFound();

    }

    public function update(User $user, AffiliateProfile $model): Response
    {
        return (bool)AffiliateProfile::findByUserId($user->id) &&
        $user->id == $model->user->id
            ? Response::allow()
            : Response::denyAsNotFound();
    }

    public function delete(User $user, AffiliateProfile $model): Response
    {
        return ((bool)AffiliateProfile::findByUserId($user->id)) ||
        $user->hasPermissionTo('delete affiliate-profiles')
            ? Response::allow()
            : Response::denyAsNotFound();

    }

    public function deleteAny(User $user): Response
    {
        return $user->hasPermissionTo('delete affiliate-profiles')
            ? Response::allow()
            : Response::denyAsNotFound();
    }

    public function restore(User $user, AffiliateProfile $model): Response
    {
        return $user->hasPermissionTo('restore affiliate-profiles')
            ? Response::allow()
            : Response::denyAsNotFound();
    }

    /**
     * Determine whether the affiliateProfile can permanently delete the model.
     */
    public function forceDelete(User $user, AffiliateProfile $model): Response
    {
        return $user->hasPermissionTo('delete affiliate-profiles')
            ? Response::allow()
            : Response::denyAsNotFound();

    }
}
