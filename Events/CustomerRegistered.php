<?php
declare(strict_types=1);

namespace Modules\ExtendedProfile\Events;

use Illuminate\Foundation\Events\Dispatchable;

class CustomerRegistered
{
    use Dispatchable;

    public function __construct()
    {
    }
}
