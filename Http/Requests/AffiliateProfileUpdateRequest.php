<?php

namespace Modules\ExtendedProfile\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AffiliateProfileUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        $codice_fiscale = $this->request->get('codice_fiscale');
        $partita_iva = $this->request->get('partita_iva');

        return [
            'codice_fiscale' => [
                'required',
                Rule::unique('affiliate_profiles', 'codice_fiscale')->ignore(
                    $codice_fiscale, 'codice_fiscale'
                ),
                'max:16',
                'string',
                'codice_fiscale'
            ],
            'partita_iva' => [
                'nullable',
                Rule::unique('affiliate_profiles', 'partita_iva')->ignore(
                    $partita_iva, 'partita_iva'
                ),
                'max:11',
                'string',
            ],
            'indirizzo' => ['required', 'max:256', 'string'],
            'cap' => ['required', 'max:5', 'string'],
            'localita' => ['required', 'max:128', 'string'],
            'provincia' => ['required', 'max:2', 'string'],
            'descrizione' => ['required', 'max:255', 'string'],
        ];
    }
}
