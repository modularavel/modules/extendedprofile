<?php
declare(strict_types=1);
namespace Modules\ExtendedProfile\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\ExtendedProfile\Rules\CodiceAffiliatoExists;

class AffiliateProfileStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     */
    public function rules(): array
    {
        return [
            //'user_id' => ['required', 'exists:users,id',new UserDoesNotHaveAffiliateProfile],
            'codice_parent' => ['nullable', new CodiceAffiliatoExists],
            'codice_fiscale' => [
                'required',
                'unique:affiliate_profiles,codice_fiscale',
                'max:16',
                'string',
                'codice_fiscale'
            ],
            'partita_iva' => [
                'nullable',
                'unique:affiliate_profiles,partita_iva',
                'max:11',
                'string',
            ],
            'indirizzo' => ['required', 'max:256', 'string'],
            'cap' => ['required', 'max:5', 'string'],
            'localita' => ['required', 'max:128', 'string'],
            'provincia' => ['required', 'max:2', 'string'],
            'descrizione' => ['required', 'max:255', 'string'],
        ];
    }
}
