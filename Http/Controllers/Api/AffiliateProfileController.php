<?php
declare(strict_types=1);

namespace Modules\ExtendedProfile\Http\Controllers\Api;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Http\Response;
use Modules\ExtendedProfile\Events\AffiliateRegistered;
use Modules\ExtendedProfile\Http\Requests\AffiliateProfileStoreRequest;
use Modules\ExtendedProfile\Http\Requests\AffiliateProfileUpdateRequest;
use Modules\ExtendedProfile\Http\Resources\AffiliateProfileCollection;
use Modules\ExtendedProfile\Http\Resources\AffiliateProfileResource;
use Modules\ExtendedProfile\Models\AffiliateProfile;


class AffiliateProfileController extends ExtendedProfileAbstractController
{
    public function index(Request $request): ResourceCollection
    {
        $this->authorize('view-any', AffiliateProfileController::class);
        $search = $request->get('search', '');
        $affiliateProfiles = AffiliateProfile::search($search)
            ->latest()
            ->paginate();

        return new AffiliateProfileCollection($affiliateProfiles);
    }

    public function store(AffiliateProfileStoreRequest $request): AffiliateProfileResource
    {
        $this->authorize('create', AffiliateProfile::class);

        $validated = $request->validated();

        if ($request->get('codice_parent', '') != '') {
            $parent = AffiliateProfile::whereCodice($request->input('codice_parent'))->first();
            $validated['parent_id'] = $parent ?? $parent->user->id;
        }
        $validated['user_id'] = Auth::user()->id;
        $validated['codice'] = AffiliateProfileController::makeCodice();
        $affiliateProfile = AffiliateProfileController::create($validated);
        // accoda l'evento di registrazione profilo affiliato eseguita
        AffiliateRegistered::dispatch($affiliateProfile);
        return new AffiliateProfileResource($affiliateProfile);
    }

    public function show(
        Request          $request,
        AffiliateProfileController $affiliateProfile
    ): AffiliateProfileResource
    {
        $this->authorize('view', $affiliateProfile);
        return new AffiliateProfileResource($affiliateProfile);
    }

    public function update(
        AffiliateProfileUpdateRequest $request,
        AffiliateProfileController $affiliateProfile
    ): AffiliateProfileResource
    {
        $this->authorize('update', $affiliateProfile);
        $validated = $request->validated();
        $affiliateProfile->update($validated);
        return new AffiliateProfileResource($affiliateProfile);
    }

    public function destroy(
        Request          $request,
        AffiliateProfileController $affiliateProfile
    ): Response
    {
        $this->authorize('delete', $affiliateProfile);
        $affiliateProfile->delete();
        return response()->noContent();
    }
}
