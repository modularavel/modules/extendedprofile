<?php
declare(strict_types=1);
namespace Modules\ExtendedProfile\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\ExtendedProfile\Http\Resources\AffiliateProfileCollection;
use Modules\ExtendedProfile\Http\Resources\AffiliateProfileResource;
use Modules\ExtendedProfile\Models\AffiliateProfile;

class AffiliateProfileAffiliateProfilesController extends Controller
{
    public function index(
        Request          $request,
        AffiliateProfile $affiliateProfile
    ): AffiliateProfileCollection
    {
        $this->authorize('view', $affiliateProfile);

        $search = $request->get('search', '');

        $childrenAffiliateProfiles = $affiliateProfile
            ->childrenAffiliateProfiles()
            ->search($search)
            ->latest()
            ->paginate();

        return new AffiliateProfileCollection($childrenAffiliateProfiles);
    }

    public function store(
        Request          $request,
        AffiliateProfile $affiliateProfile
    ): AffiliateProfileResource
    {
        $this->authorize('create', AffiliateProfile::class);

        $validated = $request->validate([
            'user_id' => ['required', 'exists:users,id'],
            'codice_fiscale' => [
                'required',
                'unique:affiliate_profiles,codice_fiscale',
                'max:16',
                'string',
            ],
            'partita_iva' => [
                'nullable',
                'unique:affiliate_profiles,partita_iva',
                'max:11',
                'string',
            ],
            'indirizzo' => ['required', 'max:256', 'string'],
            'cap' => ['required', 'max:5', 'string'],
            'localita' => ['required', 'max:128', 'string'],
            'provincia' => ['required', 'max:2', 'string'],
            'descrizione' => ['required', 'max:255', 'string'],
        ]);

        $affiliateProfile = $affiliateProfile
            ->affiliateProfiles()
            ->create($validated);

        return new AffiliateProfileResource($affiliateProfile);
    }
}
