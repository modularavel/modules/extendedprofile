<?php
declare(strict_types=1);

namespace Modules\ExtendedProfile\Http\Controllers\Api;

use Modules\IRCore\Http\Controllers\Api\CoreAbstractiController;

abstract class ExtendedProfileAbstractController extends CoreAbstractiController
{
}
