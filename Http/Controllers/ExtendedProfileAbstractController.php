<?php
declare(strict_types=1);

namespace Modules\ExtendedProfile\Http\Controllers;

use Modules\IRCore\Http\Controllers\CoreAbstractController;

abstract class ExtendedProfileAbstractController extends CoreAbstractController
{
}
