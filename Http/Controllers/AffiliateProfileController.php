<?php
declare(strict_types=1);

namespace Modules\ExtendedProfile\Http\Controllers;

use Auth;
use Illuminate\Contracts\Foundation\Application as ContractsApplication;
use Illuminate\Contracts\View\Factory;
use Illuminate\Foundation\Application as ApplicationFoudation;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Modules\ExtendedProfile\Http\Requests\AffiliateProfileStoreRequest;
use Modules\ExtendedProfile\Http\Requests\AffiliateProfileUpdateRequest;
use Modules\ExtendedProfile\Models\AffiliateProfile;

class AffiliateProfileController extends ExtendedProfileAbstractController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(Request $request): ContractsApplication|Factory|View|ApplicationFoudation
    {
        $this->authorize('view-any', AffiliateProfile::class);

        $search = $request->get('search', '');

        $affiliateProfiles = AffiliateProfile::search($search)
            ->latest()
            ->paginate(5)
            ->withQueryString();

        return view(
            'extendedprofile::app.affiliate_profiles.index',
            compact('affiliateProfiles', 'search')
        );
    }

    public function store(AffiliateProfileStoreRequest $request): RedirectResponse
    {
        $this->authorize('create', AffiliateProfile::class);

        $validated = $request->validated();
        $validated['user_id'] = Auth::user()->id;

        $parentAffiliate = AffiliateProfile::whereCodice($validated['codice_parent'])->first();
        $validated['parent_affiliate_profile_id'] = $parentAffiliate?->id;
        $validated['codice'] = AffiliateProfile::makeCodice();
        $affiliateProfile = AffiliateProfile::create($validated);

        return redirect()
            ->route('affiliate-profiles.edit', $affiliateProfile)
            ->withSuccess(__('crud.common.created'));
    }

    public function create(Request $request): ContractsApplication|Factory|\Illuminate\Contracts\View\View|ApplicationFoudation
    {
        $this->authorize('create', AffiliateProfile::class);
        return view('extendedprofile::app.affiliate_profiles.create');
    }

    public function show(Request $request, AffiliateProfile $affiliateProfile): ContractsApplication|Factory|\Illuminate\Contracts\View\View|ApplicationFoudation
    {
        $this->authorize('view', $affiliateProfile);
        return view('extendedprofile::app.affiliate_profiles.show', compact('affiliateProfile'));
    }

    public function edit(Request $request, AffiliateProfile $affiliateProfile): ContractsApplication|Factory|\Illuminate\Contracts\View\View|ApplicationFoudation
    {
        $this->authorize('update', $affiliateProfile);
        return view(
            'extendedprofile::app.affiliate_profiles.edit',
            compact('affiliateProfile')
        );
    }

    public function update(AffiliateProfileUpdateRequest $request, AffiliateProfile $affiliateProfile): RedirectResponse
    {
        $this->authorize('update', $affiliateProfile);
        $validated = $request->validated();
        $affiliateProfile->update($validated);
        return redirect()
            ->route('affiliate-profiles.edit', $affiliateProfile)
            ->withSuccess(__('crud.common.saved'));
    }

    public function destroy(Request $request, AffiliateProfile $affiliateProfile): RedirectResponse
    {
        $this->authorize('delete', $affiliateProfile);
        $affiliateProfile->delete();
        return redirect()
            ->route('affiliate-profiles.index')
            ->withSuccess(__('crud.common.removed'));
    }
}
