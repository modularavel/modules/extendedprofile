<?php
declare(strict_types=1);

namespace Modules\ExtendedProfile\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class CodiceCustomerExists implements ValidationRule
{
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (trim($value) != '') {
            return;
        }
        if (!CustomerProfile::codiceExists($value)) {
            $fail('Codice parent non trovato. Inserire un codice esistente o lasciare vuoto il campo.');
        }
    }
}
