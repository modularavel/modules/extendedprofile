<?php
declare(strict_types=1);

namespace Modules\ExtendedProfile\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Modules\ExtendedProfile\Models\User;

class UserDoesNotHaveAffiliateProfile implements ValidationRule
{
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $user = User::findOrFail($value);
        if ($user->hasAffiliateProfile()) {
            $fail(__('extendedprofile::crud.affiliate_profiles.already_has_profile'));
        }
    }
}
