<?php
declare(strict_types=1);

namespace Modules\ExtendedProfile\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;
use Modules\ExtendedProfile\Models\AffiliateProfile;

class CodiceAffiliatoExists implements ValidationRule
{
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        if (trim($value) != '') {
            return;
        }
        if (!AffiliateProfile::codiceExists($value)) {
            $fail(__("extendedprofile::crud.affiliate_profiles.invalid_parent_code", ['codice'=>$value]));
        }
    }
}
