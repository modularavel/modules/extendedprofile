<?php
declare(strict_types=1);

namespace Modules\ExtendedProfile\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\ExtendedProfile\Database\Factories\AffiliateProfileFactory;
use Modules\IRCore\Models\EventAwareModel;
use Modules\IRCore\Observers\DataHistoryEnabledModelObserver;
use Spatie\Permission\Traits\HasRoles;

class CustomerProfile extends EventAwareModel
{
    use HasRoles;
    use HasFactory;
    use Searchable;
    use SoftDeletes;

    protected const codiceAllowedChars = 'ABCDEFGHJLMNPQRSTUVWYZ23456789';
    protected const codiceLength = 12;
    protected $fillable = [
        'user_id',
        'parent_customer_profile_id',
        'reference_affiliate_profile_id',
        'codice_fiscale',
        'partita_iva',
        'indirizzo',
        'cap',
        'localita',
        'provincia',
        'descrizione',
        'codice'
    ];
    protected $searchableFields = ['*'];
    protected $table = 'customer_profiles';

    public static function getIdFromUserId($userId): int
    {
        $profile = self::findByUserId($userId);
        return $profile ? $profile->id : 0;
    }

    public static function findByUserId(int $id)
    {
        return self::whereUserId($id)->first();
    }

    public static function makeCodice(): string
    {
        $codice = self::makeRandomString();
        while (self::codiceExists($codice)) {
            $codice = self::makeRandomString();
        }
        return $codice;
    }

    protected static function makeRandomString(): string
    {
        $str = '';
        $maxCharInd = strlen(self::codiceAllowedChars) - 1;
        for ($i = 0; $i < self::codiceLength; $i++) {
            $str .= self::codiceAllowedChars[rand(0, $maxCharInd)];
        }
        return $str;
    }

    public static function codiceExists(string $codice): bool
    {
        return (bool)(self::whereCodice($codice)
            ->firstOr(function () {
                return false;
            }
            ));
    }

    protected static function booted()
    {
      //  self::observe(DataHistoryEnabledModelObserver::class);
    }

    protected static function newFactory()
    {
        return AffiliateProfileFactory::new();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function parentCustomerProfile(): BelongsTo
    {
        return $this->belongsTo(CustomerProfile::class, 'id', 'parent_customer_profile_id');
    }

    public function childrenCustomerProfiles(): HasMany
    {
        return $this->hasMany(CustomerProfile::class, 'id', 'parent_customer_profile_id');
    }

    public function referenceAffiliateProfile(): BelongsTo
    {
        return $this->belongsTo(AffiliateProfile::class, 'id', 'reference_affiliate_profile_id');
    }
}
