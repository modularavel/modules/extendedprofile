<?php

namespace Modules\ExtendedProfile\Models;

use Auth;

class User extends \App\Models\User
{
    public static function current(): User
    {
        $user = Auth::user();
        return User::findOrFail($user->id);
    }

    public function AffiliateProfile()
    {
        return $this->hasOne(AffiliateProfile::class);
    }

    public function hasAffiliateProfile(): bool
    {
        return (bool)$this->AffiliateProfile;
    }

    /**
     * Add/Attach a profile to a user.
     *
     * @param AffiliateProfile $profile
     */
    public function assignAffiliateProfile(AffiliateProfile $profile)
    {
        return $this->affiliateProfiles()->attach($profile);
    }

    /**
     * Remove/Detach a profile to a user.
     *
     * @param Profile $profile
     */
    public function removeAffiliateProfile(AffiliateProfile $profile)
    {
        return $this->affiliateProfiles()->detach($profile);
    }
}
